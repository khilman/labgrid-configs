#
# copy/paste of UBootStrategy.py, extended to do TFTP boot
#
import logging
import enum
import attr
import socket

from labgrid.factory import target_factory
from labgrid.strategy.common import Strategy, StrategyError


class Status(enum.Enum):
    unknown = 0
    off = 1
    uboot = 2
    shell = 3
    tftpboot = 4

@target_factory.reg_driver
@attr.s(eq=False)
class MyUBootStrategy(Strategy):
    """UbootStrategy - Strategy to switch to uboot or shell"""
    bindings = {
        "power": "PowerProtocol",
        "console": "ConsoleProtocol",
        "uboot": "UBootDriver",
        "shell": "ShellDriver",
        "tftp": "TFTPProviderDriver",
    }

    boot = attr.ib(validator=attr.validators.instance_of(dict))
    status = attr.ib(default=Status.unknown)

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.logger = logging.getLogger("S{self}:{self.target}")
        self.hostname = socket.gethostname()
        self.host_ip = socket.gethostbyname(self.hostname)
        self.tftp_files = {}
        
    def transition(self, status):
        self.logger.info("transition: status={}".format(status))

        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError(f"can not transition to {status}")
        elif status == self.status:
            return # nothing to do
        elif status == Status.off:
            self.target.deactivate(self.console)
            self.target.activate(self.power)
            self.target.activate(self.tftp)
            self.power.off()
        elif status == Status.uboot:
            self.transition(Status.off)  # pylint: disable=missing-kwoa
            self.target.activate(self.console)
            # cycle power
            self.power.cycle()
            # interrupt uboot
            self.target.activate(self.uboot)

            # prepare network
            self.uboot.run("env set autoload no; env set autoboot no")
            self.uboot.run("dhcp")
            self.uboot.run(f"env set serverip {self.host_ip}")
        elif status == Status.shell:
            # tansition to uboot
            self.transition(Status.uboot)
            self.uboot.boot("")
            self.uboot.await_boot()
            self.target.activate(self.shell)

        elif status == Status.tftpboot:
            # tansition to uboot
            self.transition(Status.uboot)
            #self.transition(Status.off)

            #self.logger.info(f"{status}: boot items={self.boot.items()}")
            for image_name, addr in self.boot.items():
                try:
                    local_path = self.target.env.config.get_image_path(image_name)
                except KeyError:
                    self.logger.warn(f"{status}: Address for {image_name} not found")
                    self.logger.warn(f"addr = {addr}")
                    continue
                
                staged_path = self.tftp.stage(local_path)
                self.logger.info(f"{status}: TFTP {image_name} to 0x{addr:08x} from {staged_path}")
                self.uboot.run(f"tftp 0x{addr:08x} {staged_path}")
                out = self.uboot.run("env print filesize")
                filesize = int(out[0][0].split('=')[1], 16)
                self.logger.info(f"{status}: {image_name} size = {filesize} (0x{filesize:x})")

                self.tftp_files[image_name] = (addr, filesize)

            t = self.tftp_files
            self.uboot.run(f"env set bootcmd bootz "
                           f"0x{t['kernel'][0]:08x} "
                           f"0x{t['initramfs'][0]:08x}:{t['initramfs'][1]:08x} "
                           f"0x{t['dtb'][0]:08x}")

            self.uboot.run("env print bootargs")
            self.uboot.run("env print bootcmd")
            self.transition(Status.shell)
            
        else:
            raise StrategyError(f"no transition found from {self.status} to {status}")
        self.status = status
